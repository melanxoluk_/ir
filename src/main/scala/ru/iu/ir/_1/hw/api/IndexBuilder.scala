package ru.iu.ir._1.hw.api

trait IndexBuilder {
  def build(
    tokenize: Document => List[String],
    norm: String => String,
    documents: List[Document]) : InvertedIndex
}
