package ru.iu.ir._1.hw.api

trait Corpus {
  def getDocuments: List[Document]
}
