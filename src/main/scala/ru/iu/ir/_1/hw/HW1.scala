package ru.iu.ir._1.hw

import ru.iu.ir._1.hw.api.{Document, InvertedIndex}


object HW1 extends App {

  // flow of program
  val index1 = SimpleIndexBuilder.build(tokenize, normalize, Corpus1.getDocuments)
  val index2 = SimpleIndexBuilder.build(tokenize, normalize, Corpus2.getDocuments)

  printIndex(index1, "Ex 1.1 index")
  printIndex(index2, "Ex 1.2 index")



  def tokenize(document: Document): List[String] = document.text.split(" ").toList

  def normalize(s: String): String = s.toLowerCase

  def printIndex(index: InvertedIndex, msg: String): Unit = {
    println(msg)

    index.lexicon.foreach(
      lex => {
        print(lex.word + ": "); lex.postings.foreach(p => print(p + " ")); println
      })

    println
  }
}












