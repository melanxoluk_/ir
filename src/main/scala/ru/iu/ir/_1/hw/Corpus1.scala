package ru.iu.ir._1.hw

import ru.iu.ir._1.hw.api.{Corpus, Document}


// Exercise 1.1 corpus
object Corpus1 extends Corpus {

  override def getDocuments: List[Document] = List(
    Document(1, "new home sales top forecasts"),
    Document(2, "home sales rise in july"),
    Document(3, "increase in home sales in july"),
    Document(4, "july new home sales rise")
  )
}
