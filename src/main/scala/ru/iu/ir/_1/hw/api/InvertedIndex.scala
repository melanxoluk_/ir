package ru.iu.ir._1.hw.api

import scala.collection.mutable.ArrayBuffer

class InvertedIndex {
  val lexicon = ArrayBuffer.empty[Token]

  def add(word: String, docId: Int): Unit = {
    val token = lexicon.find(t => t.word == word)
    val empty = token.isEmpty

    if (empty) {
      val token = Token(word)
      token.postings += docId
      lexicon += token
    } else {
      token.get.postings += docId
    }
  }
}
