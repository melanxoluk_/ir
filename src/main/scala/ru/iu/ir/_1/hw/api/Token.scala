package ru.iu.ir._1.hw.api

import scala.collection.mutable.SortedSet

case class Token(word: String) {
  val postings = SortedSet.empty[Int]
}
