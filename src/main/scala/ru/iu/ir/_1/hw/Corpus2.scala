package ru.iu.ir._1.hw

import ru.iu.ir._1.hw.api.{Corpus, Document}


// Exercise 1.2 corpus
object Corpus2 extends Corpus {

  override def getDocuments: List[Document] = List(
    Document(1, "breakthrough drug for schizophrenia"),
    Document(2, "new schizophrenia drug"),
    Document(3, "new approach for treatment of schizophrenia"),
    Document(4, "new hopes for schizophrenia patients")
  )
}
