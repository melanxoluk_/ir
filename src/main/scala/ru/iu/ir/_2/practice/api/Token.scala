package ru.iu.ir._2.practice.api

import scala.collection.mutable.SortedSet

case class Token(word: String) {
  val postings = SortedSet.empty[Int]
}
