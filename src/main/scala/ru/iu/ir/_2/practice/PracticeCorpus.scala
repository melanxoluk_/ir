package ru.iu.ir._2.practice

import ru.iu.ir._2.practice.api.{Corpus, Document}


object PracticeCorpus extends Corpus {
  override def getDocuments: List[Document] = List(
    Document(1, "Apple Inc. is an American multinational " +
      "technology company headquartered in Cupertino, California"),

    Document(2, "A typical apple serving weights 242 grams and " +
      "provides 126 calories with"),

    Document(3, "")
  )
}
