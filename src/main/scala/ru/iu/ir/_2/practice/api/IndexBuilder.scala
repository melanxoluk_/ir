package ru.iu.ir._2.practice.api

trait IndexBuilder {
  def build(
    tokenize: Document => List[String],
    norm: String => String,
    documents: List[Document]) : InvertedIndex
}
