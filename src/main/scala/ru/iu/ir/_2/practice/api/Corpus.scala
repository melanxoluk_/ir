package ru.iu.ir._2.practice.api

import ru.iu.ir._1.hw.api.Document

trait Corpus {
  def getDocuments: List[Document]
}
