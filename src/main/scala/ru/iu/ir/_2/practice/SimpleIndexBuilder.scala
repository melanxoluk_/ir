package ru.iu.ir._2.practice

import ru.iu.ir._2.practice.api.{Document, IndexBuilder, InvertedIndex}


object SimpleIndexBuilder extends IndexBuilder {

  override def build(
    tokenize: (Document) => List[String],
    norm: (String) => String,
    documents: List[Document]): InvertedIndex = {

    val index = new InvertedIndex

    documents
      .map(d => Tuple2(d.id, tokenize(d)))
      .foreach(t => t._2.foreach(w => index.add(norm(w), t._1)))

    index
  }
}
