package ru.iu.ir._2.practice

import ru.iu.ir._2.practice.api.{Document, InvertedIndex}


object Practice2 extends App {

  val index = SimpleIndexBuilder.build(tokenize, normalize, PracticeCorpus.getDocuments)
  printIndex(index, "Practice index")


  def tokenize(document: Document): List[String] = document.text.split(" ").toList

  def normalize(s: String): String = s.toLowerCase

  def printIndex(index: InvertedIndex, msg: String): Unit = {
    println(msg)

    index.lexicon.foreach(
      lex => {
        print(lex.word + ": "); lex.postings.foreach(p => print(p + " ")); println
      })

    println
  }
}
